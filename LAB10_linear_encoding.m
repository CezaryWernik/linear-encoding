close all
clear all
clc

%% dane do ustawienia
%do test�w dane pozostaw pocz�tkowo jak w oryginale
%nast�pnie zmie� dane na sw�j nr album zapisany binarnie
data = [1 0 1 0 1 1 0 0 1 0 0 0 1 1 0 0 0];%strumie� danych

%Bod (ang. Baud), typowe warto�ci: 1200/2400/4800/9600
bod=10;

%% zmienne pomocnicze
sim_fs=bod*100; %cz�stotliwo�� pr�bkowania dla symulacji 100 wi�ksza od Bod
sp_clk=sim_fs/bod; %sampli na cykl zegara
N = length(data); %ilo�� bit�w w danych wej�ciowych
sp_d=N*sp_clk; %sampli na ilo�� danych
t = 0:(1/sim_fs):((sp_d/sim_fs)-(1/sim_fs)); %czas trwania sygna�u
empty = zeros(1,length(t));%pomocniczy pusty wektor 

CLK     = clkGenerator(sp_clk,sp_d);%sygna� zegara

%% zadanie w�a�ciwe
%strumie� po kodowaniu
%gdy napiszesz w�asn� funkcj� koduj�c� zamie� "empty" na w�a�ciw� nazw� funkcji
TTL     = empty;%ttlGenerator(CLK,data);
MAN     = empty;%manchesterGenerator(CLK,data);
NRZI    = empty;%nrziGenerator(CLK,data);
BAMI    = empty;%bamiGenerator(CLK,data);

%% pomocnicza funkcja rysuj�ca
plotLinearEncodeSignal(t, CLK, TTL, MAN, NRZI, BAMI);