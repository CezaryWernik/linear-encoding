function [clk] = clkGenerator(samplePerCLK,samplePerData)

clk=zeros(1,samplePerData);
ticToc=1;
for n=1:samplePerData
    if mod(n,samplePerCLK/2)==0
        ticToc=~ticToc;
    end
    clk(n)=ticToc;
end

end

